<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ForgetPassword;
use App\Models\User;
use App\Models\DataCultivation;
use App\Models\DataBuyingFarm;
use App\Models\DataBuyingDistributor;
use App\Models\Role;
use Ramsey\Uuid\Uuid;
use Storage; 
use Auth;
use Response;
use Validator;
use DB;
use Mail;
use Hash;
use Carbon\Carbon;

class RestController extends Controller
{
    
    public function Register(Request $request)
    {
        $response = array();
        $input = $request->all();
        $message= [
            'nama.required'=>'Mohon isi Nama Anda',
            'address.required'=>'Mohon isi alamat Anda',
            'lat.required'=>'Mohon pilih lokasi Anda',
            'lng.required'=>'Mohon pilih lokasi Anda',
            'mitra.required'=>'Mohon pilih jenis kemitraan Anda',
            'email.required'=>'Mohon isi Email Anda',
            'email.email'=>'Mohon isi Email Anda dengan benar',
            'email.unique'=>'Email Sudah Terdaftar',
            'number_phone.required'=>'Mohon isi Nomor Handphone',
            'number_phone.numeric'=>'Mohon isi Nomor Handphone dengan angka',
            'number_phone.unique'=>'Nomor Handphone sudah terdaftar',
            'password.required'=>'Mohon isi Password',
            'password.min'=>'Password minimal 8 karakter',
            'password.regex'=>'Password Harus ada huruf besar dan kecil, angka dan simbol',
        ];
        $valid = [
            'nama' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'mitra' => 'required',
            'email' => 'required|email|unique:users',
            'number_phone' => 'required|numeric|unique:users',
            'password' => 'required|min:8|',
            // 'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@&*)(_$#%+^><=)(]).*$/',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            $token = mt_rand(111111, 999999);
            $data = [
                'name' => $request->nama,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'number_phone' => $request->number_phone,
                'address'=>$request->address,
                'lat'=>$request->lat,
                'lng'=> $request->lng,
                'idrole'=>$request->mitra,
            ];
            $data2 = [
                'retype_password' => $request->password,
                'token'=> $token,
                'status'=>1,
            ];
            $return = array_merge($data,$data2);
            $user = User::create($return);
            if ($user) {
                Mail::send('email.activation', ['token' => $token], function($message) use($request){
                    $message->to($request->email);
                    $message->subject('Email Aktivasi');
                });
                $response['code'] = 200;
                $response['data'] = $data;
                $response['message'] = 'Berhasil Register';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Server sedang sibuk';
            }
        }
        return response()->json($response, $response['code']);
        
    }
    public function Activation(Request $request)
    {
        $response = array();
        $input = $request->all();
        $message= [
            'token.required'=>'Mohon Token Anda',
        ];
        $valid = [
            'token' => 'required',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            $data = [
                'status' => 1,
                'email_verified_at' => Carbon::now(),
            ];
            $user = User::where('token',$request->token)->update($data);
            if ($user) {
                $response['code'] = 200;
                $response['message'] = 'Berhasil Register';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Server sedang sibuk';
            }
        }
        return response()->json($response, $response['code']);
        
    }
    public function forgetPassword(Request $request)
    {
        DB::beginTransaction();
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);
        
        $token = mt_rand(111111, 999999);
        
        Mail::send('email.forgetpass', ['token' => $token], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });
        $reset = ForgetPassword::insert([
            'email' => $request->email, 
            'token' => $token, 
            'created_at' => Carbon::now()
        ]);
        
        if ($reset) {
            DB::commit();
            $response['code'] = 200;
            $response['message'] = 'Token di kirim ke email Anda';
            
        }else{
            DB::rollBack();
            $response['code'] = 400;
            $response['message'] = 'Data gagal Kirim';
        }
        return response()->json($response, $response['code']);
    }
    public function Login(Request $request)
    {
        $response = array();
        $input = $request->all();
        $message = [
            'email.required'=>'Mohon isi email anda',
            'password.required'=>'Mohon isi password anda',
        ];
        $valid = [
            'email' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            $datalogin = [
                'email'=>$request->email,
                'password'=>$request->password,
                'status'=>'1',
            ];
            Auth::attempt($datalogin);
            if (Auth::check()) {
                $datauser = [
                    'nama' => Auth::user()->nama,
                    'email' => Auth::user()->email,
                    'number_phone' => Auth::user()->number_phone,
                    'address'=>Auth::user()->address,
                    'lat'=>Auth::user()->lat,
                    'lng'=> Auth::user()->lng,
                    'idrole'=>Auth::user()->mitra,
                ];
                $response['code'] = 200;
                $response['token'] = User::find(Auth::user()->id)->createToken('RestData')->plainTextToken;
                $response['data'] = $datauser;
                $response['message'] = 'Berhasil Login';
            }else{
                $response['code'] = 400;
                $response['message'] = 'username atau password salah';
            }
        }
        return response()->json($response, $response['code']);
    }
    public function getProfile()
    {
        $response = [];
        if(Auth::user()->id == null){
            $response['code'] = 401;
            $response['message'] = 'Unauthorized';
        }else{
            $response['code'] = 200;
            $response['message'] = 'Berhasil';
            $response['data'] = Auth::user();
        }
        return response()->json($response, $response['code']);
    }
    public function SaveDataCultivation(Request $request)
    {
        $response = array();
        $input = $request->all();
        if (@$request->id == null) {
            $response['code'] = 200;
            $response['data'] = Uuid::uuid4()->getHex();
            $response['message'] = 'Berhasil';

            return response()->json($response, $response['code']);
        }
        $message = [
            'id.required'=>'Mohon isi id unik',
            // 'luas_lahan.required'=>'Mohon isi luas lahan',
            // 'jenis_produksi.required'=>'Mohon pilih',
            // 'jumlah_biji.required'=>'Mohon isi jumlah biji perlubang',
            // 'jumlah_urea.required'=>'Mohon isi jumlah pemupukan urea',
            // 'jumlah_sp36.required'=>'Mohon isi jumlah pemupukan pupuk SP36',
            // 'jumlah_kci.required'=>'Mohon isi jumlah pemupukan KCI',
            // 'jumlah_pupuk_kandang.required'=>'Mohon isi jumlah pemupukan pupuk kandang',
            // 'tanggal_awal.required'=>'Mohon isi tanggal awal perkiraan siap panen',
            // 'tanggal_akhir.required'=>'Mohon isi tanggal akhir perkiraan siap panen',
            // 'id_pabrik.required'=>'Mohon pilih pabrik terdekat',
            // 'tanggal_panen.required'=>'Mohon isi tanggal panen',
            'file_lahan.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_tanam.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_pupuk.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_penyiangan.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_pembubuhan.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_pengairan.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_panen.mimes'=>'Pastikan Format File jpeg,png atau jpg',
            'file_lahan.max'=>'Maksimal Ukuran File 5MB',
            'file_tanam.max'=>'Maksimal Ukuran File 5MB',
            'file_pupuk.max'=>'Maksimal Ukuran File 5MB',
            'file_penyiangan.max'=>'Maksimal Ukuran File 5MB',
            'file_pembubuhan.max'=>'Maksimal Ukuran File 5MB',
            'file_pengairan.max'=>'Maksimal Ukuran File 5MB',
            'file_panen.max'=>'Maksimal Ukuran File 5MB',
        ];
        $valid = [
            'id' => 'required',
            // 'luas_lahan' => 'required',
            // 'jenis_produksi' => 'required',
            // 'jumlah_biji' => 'required',
            // 'jumlah_urea' => 'required',
            // 'jumlah_sp36' => 'required',
            // 'jumlah_kci' => 'required',
            // 'jumlah_pupuk_kandang' => 'required',
            // 'tanggal_awal' => 'required',
            // 'tanggal_akhir' => 'required',
            // 'id_pabrik' => 'required',
            // 'tanggal_panen' => 'required',
            'file_lahan'=>'mimes:jpg,jpeg,png|max:5048',
            'file_tanam'=>'mimes:jpg,jpeg,png|max:5048',
            'file_pupuk'=>'mimes:jpg,jpeg,png|max:5048',
            'file_penyiangan'=>'mimes:jpg,jpeg,png|max:5048',
            'file_pembubuhan'=>'mimes:jpg,jpeg,png|max:5048',
            'file_pengairan'=>'mimes:jpg,jpeg,png|max:5048',
            'file_panen'=>'mimes:jpg,jpeg,png|max:5048',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            if ($request->hasFile('file_lahan')) {
                $namelahan = md5($request->file_lahan->getClientOriginalName().time()).'.'.$request->file_lahan->extension();
                $save = $request->file_lahan->storeAs('public/file_uploads',$namelahan);
                $urllahan = env('APP_URL').'/public/storage/file_uploads/'.$namelahan;
                $linklahan = env('IMAGEKIT_ENDPOINT').'/'.$namelahan;
                $filelahan = Storage::disk('imagekit')->put($namelahan,$urllahan);
            }
            
            if ($request->hasFile('file_tanam')) {
                $nametanam = md5($request->file_tanam->getClientOriginalName().Carbon::now()).'.'.$request->file_tanam->extension();
                $save = $request->file_tanam->storeAs('public/file_uploads',$nametanam);
                $urltanam = env('APP_URL').'/public/storage/file_uploads/'.$nametanam;
                $linktanam = env('IMAGEKIT_ENDPOINT').'/'.$nametanam;
                $filetanam = Storage::disk('imagekit')->put($nametanam,$urltanam);
            }
            
            if ($request->hasFile('file_pupuk')) {
                $namepupuk = md5($request->file_pupuk->getClientOriginalName().Carbon::now()).'.'.$request->file_pupuk->extension();
                $save = $request->file_pupuk->storeAs('public/file_uploads',$namepupuk);
                $urlpupuk = env('APP_URL').'/public/storage/file_uploads/'.$namepupuk;
                $linkpupuk = env('IMAGEKIT_ENDPOINT').'/'.$namepupuk;
                $filepupuk = Storage::disk('imagekit')->put($namepupuk,$urlpupuk);
            }
            
            if ($request->hasFile('file_penyiangan')) {
                $namepenyiangan = md5($request->file_penyiangan->getClientOriginalName().Carbon::now()).'.'.$request->file_penyiangan->extension();
                $save = $request->file_penyiangan->storeAs('public/file_uploads',$namepenyiangan);
                $urlpenyiangan = env('APP_URL').'/public/storage/file_uploads/'.$namepenyiangan;
                $linkpenyiangan = env('IMAGEKIT_ENDPOINT').'/'.$namepenyiangan;
                $filepenyiangan = Storage::disk('imagekit')->put($namepenyiangan,$urlpenyiangan);
            }
            
            if ($request->hasFile('file_pembubuhan')) {
                $namepembubuhan = md5($request->file_pembubuhan->getClientOriginalName().Carbon::now()).'.'.$request->file_pembubuhan->extension();
                $save = $request->file_pembubuhan->storeAs('public/file_uploads',$namepembubuhan);
                $urlpembubuhan = env('APP_URL').'/public/storage/file_uploads/'.$namepembubuhan;
                $linkpembubuhan = env('IMAGEKIT_ENDPOINT').'/'.$namepembubuhan;
                $filepembubuhan = Storage::disk('imagekit')->put($namepembubuhan,$urlpembubuhan);
            }
            
            if ($request->hasFile('file_pengairan')) {
                $namepengairan = md5($request->file_pengairan->getClientOriginalName().Carbon::now()).'.'.$request->file_pengairan->extension();
                $save = $request->file_pengairan->storeAs('public/file_uploads',$namepengairan);
                $urlpengairan = env('APP_URL').'/public/storage/file_uploads/'.$namepengairan;
                $linkpengairan = env('IMAGEKIT_ENDPOINT').'/'.$namepengairan;
                $filepengairan = Storage::disk('imagekit')->put($namepengairan,$urlpengairan);
            }
            
            if ($request->hasFile('file_panen')) {
                $namepanen = md5($request->file_panen->getClientOriginalName().Carbon::now()).'.'.$request->file_panen->extension();
                $save = $request->file_panen->storeAs('public/file_uploads',$namepanen);
                $urlpanen = env('APP_URL').'/public/storage/file_uploads/'.$namepanen;
                $linkpanen = env('IMAGEKIT_ENDPOINT').'/'.$namepanen;
                $filelahan = Storage::disk('imagekit')->put($name,$urlpanen);
            }
            
            $data = [
                'land_area'=>@$request->luas_lahan,
                'type_product'=>@$request->jenis_produksi,
                'total_seed'=>@$request->jumlah_biji,
                'total_urea'=>@$request->jumlah_urea,
                'total_sp36'=>@$request->jumlah_sp36,
                'total_kci'=>@$request->jumlah_kci,
                'total_manure'=>@$request->jumlah_pupuk_kandang,
                'date_first'=>date('Y-m-d',strtotime(@$request->tanggal_awal)),
                'date_last'=>date('Y-m-d',strtotime(@$request->tanggal_akhir)),
                'id_users'=>@$request->id_pabrik,
                'date_harvest'=>date('Y-m-d',strtotime(@$request->tanggal_panen)),
                'createdby'=>Auth::user()->id,
                
                'file_land'=>@$linklahan,
                'file_planting'=>@$linktanam,
                'file_fertilization'=>@$linkpupuk,
                'file_thinning'=>@$linkpenyiangan,
                'file_formation'=>@$linkpembubuhan,
                'file_irrigation'=>@$linkpengairan,
                'file_harvest'=>@$linkpanen,
            ];
            $idunik = [
                'uniq_id'=>$request->id
            ];
            $create = DataCultivation::updateOrCreate($idunik,$data);
            if($create){
                @unlink(@public_path().'/storage/file_uploads/'.@$namelahan);
                @unlink(@public_path().'/storage/file_uploads/'.@$nametanam);
                @unlink(@public_path().'/storage/file_uploads/'.@$namepupuk);
                @unlink(@public_path().'/storage/file_uploads/'.@$namepenyiangan);
                @unlink(@public_path().'/storage/file_uploads/'.@$namepembubuhan);
                @unlink(@public_path().'/storage/file_uploads/'.@$namepengairan);
                @unlink(@public_path().'/storage/file_uploads/'.@$namepanen);
                $response['code'] = 200;
                $response['message'] = 'Berhasil Simpan Data';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Gagal Simpan';
            }
        }
        
        return response()->json($response, $response['code']);
    }
    public function SaveDataFarm(Request $request)
    {
        $response = array();
        $input = $request->all();
        $message = [
            'id_pabrik.required'=>'Mohon Pilih Pabrik Terdekat',
            'jumlah_ternak.required'=>'Mohon isi Jumlah ternak',
            'avg_ternak.required'=>'Mohon isi berat rata-rata ternak',
            'kuantitas_makan.required'=>'Mohon isi kuantitas kebutuhan pakan',
            'tgl_kirim.required'=>'Mohon isi tanggal pengiriman',
        ];
        $valid = [
            'id_pabrik' => 'required',
            'jumlah_ternak' => 'required',
            'avg_ternak' => 'required',
            'kuantitas_makan' => 'required',
            'tgl_kirim' => 'required',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            $data = [ 
                'id_users'=>$request->id_pabrik,
                'total_cattle'=>$request->jumlah_ternak,
                'average_weight'=>$request->avg_ternak,
                'food_requirement'=>$request->kuantitas_makan,
                'date_delivery'=>date('Y-m-d',strtotime($request->tgl_kirim)),
                'createdby'=>Auth::user()->id,
            ];
            $create = DataBuyingFarm::create($data);
            if($create){
                $response['code'] = 200;
                $response['message'] = 'Berhasil Simpan Data';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Gagal Simpan';
            }
        }
        return response()->json($response, $response['code']);
    }
    public function SaveDataDistributor(Request $request)
    {
        $response = array();
        $input = $request->all();
        $message = [
            'id_pabrik.required'=>'Mohon Pilih Pabrik Terdekat',
            'jumlah_barang.required'=>'Mohon isi Jumlah ternak',
            'kuantitas_makan.required'=>'Mohon isi kuantitas kebutuhan pakan',
            'tgl_kirim.required'=>'Mohon isi tanggal pengiriman',
        ];
        $valid = [
            'id_pabrik' => 'required',
            'jumlah_barang' => 'required',
            'kuantitas_makan' => 'required',
            'tgl_kirim' => 'required',
        ];
        $validator = Validator::make($input,$valid,$message);
        if ($validator->fails()) {
            $response['code'] = 400;
            $response['message'] = $validator->errors()->first('*');
        }else{
            $data = [ 
                'id_users'=>$request->id_pabrik,
                'total_item'=>$request->jumlah_barang,
                'food_requirement'=>$request->kuantitas_makan,
                'date_delivery'=>date('Y-m-d',strtotime($request->tgl_kirim)),
                'createdby'=>Auth::user()->id,
            ];
            $create = DataBuyingDistributor::create($data);
            if($create){
                $response['code'] = 200;
                $response['message'] = 'Berhasil Simpan Data';
            }else{
                $response['code'] = 400;
                $response['message'] = 'Gagal Simpan';
            }
        }
        return response()->json($response, $response['code']);
    }

    public function getDataBuyingFarm($perPage = null, $pageName = 'page', $page = null,Request $request)
    {
        $data = DataBuyingFarm::orderBy('created_at','DESC');
        if (Auth::user()->idrole == 3) {
            $data->where('id_users',Auth::user()->id);
        }else{
            $data->where('createdby',Auth::user()->id);
        }
        $response['code'] = 200;
        $response['data'] = $data->paginate($perPage = 15, $columns = ['*'], $pageName = 'page');
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDetailBuyingFarm(Request $request)
    {
        $dataDetail = DataBuyingFarm::find($request->id);
        $response['code'] = 200;
        $response['data'] = $dataDetail;
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDataBuyingDistributor($perPage = null, $pageName = 'page', $page = null,Request $request)
    {
        $data = DataBuyingDistributor::orderBy('created_at','DESC');
        if (Auth::user()->idrole == 3) {
            $data->where('id_users',Auth::user()->id);
        }else{
            $data->where('createdby',Auth::user()->id);
        }
        $response['code'] = 200;
        $response['data'] = $data->paginate($perPage = 15, $columns = ['*'], $pageName = 'page');
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDetailBuyingDistributor(Request $request)
    {
        $dataDetail = DataBuyingFarm::find($request->id);
        $response['code'] = 200;
        $response['data'] = $dataDetail;
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDataCultivation($perPage = null, $pageName = 'page', $page = null,Request $request)
    {
        $data = DataCultivation::orderBy('created_at','DESC');
        if (Auth::user()->idrole == 3) {
            $data->where('id_users',Auth::user()->id);
        }else{
            $data->where('createdby',Auth::user()->id);
        }
        $response['code'] = 200;
        $response['data'] = $data->paginate($perPage = 15, $columns = ['*'], $pageName = 'page');
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function graphicCultivation()
    {
        $data = DataCultivation::select('users.name','users.address',DB::raw('COUNT(data_cultivation.id) as jumlah'))->join('users','users.id','=','data_cultivation.id_users')->groupBy('data_cultivation.id_users');
        $response['code'] = 200;
        $response['data'] = $data->get();
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }

    public function getDataFactory()
    {
        $user = User::select('name','lat','lng','address')->where('idrole',3);
        $response['code'] = 200;
        $response['data'] = $user->get();
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDataFarm()
    {
        $user = User::select('name','lat','lng','address')->where('idrole',2);
        $response['code'] = 200;
        $response['data'] = $user->get();
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getListUser(Request $request)
    {
        $response = [];
        $response['code'] = 200;
        $response['data'] = User::with('role')->get();
        $response['message'] = 'Berhasil';
        return response()->json($response, $response['code']);
    }
    public function getDataRole()
    {
        $response = [];
        $role = Role::get();
        $response['code'] = 200;
        $response['data'] = $role;
        return response()->json($response, 200);
    }
}
