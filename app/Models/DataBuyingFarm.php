<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataBuyingFarm extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'data_buying_farm';
    protected $fillable = ['id_users','total_cattle','average_weight','food_requirement','date_delivery','createdby'];
}
