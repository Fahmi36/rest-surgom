<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataBuyingDistributor extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'data_buying_distributor';
    protected $fillable = ['id_users','total_item','food_requirement','date_delivery','createdby'];
}
