<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataCultivation extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'data_cultivation';
    protected $fillable = ['land_area','type_product','total_seed','total_urea','total_sp36','total_kci','total_manure','date_first','date_last','id_users','date_harvest','createdby','file_land','file_planting','file_fertilization','file_thinning','file_formation','file_irrigation','file_harvest'];
}
