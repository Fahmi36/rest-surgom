<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'Api\RestController@Register')->name('register');
Route::post('activation', 'Api\RestController@Activation')->name('activation');
Route::post('forgetpass', 'Api\RestController@forgetPassword')->name('forget');
Route::post('login', 'Api\RestController@Login')->name('login');
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('SimpanDataBudidaya', 'Api\RestController@SaveDataCultivation')->name('SaveDataCultivation');
    Route::post('SimpanDataPeternakan', 'Api\RestController@SaveDataFarm')->name('SaveDataFarm');
    Route::post('SimpanDataDistributor', 'Api\RestController@SaveDataDistributor')->name('SaveDataDistributor');
    Route::get('myProfile', 'Api\RestController@getProfile');
    Route::get('ambilDataRole', 'Api\RestController@getDataRole');
    Route::get('ambilDataPembelianPetani', 'Api\RestController@getDataBuyingFarm');
    Route::post('detailDataPembelianPetani', 'Api\RestController@getDetailBuyingFarm');
    Route::get('ambilDataPembelianDistrbutor', 'Api\RestController@getDataBuyingDistributor');
    Route::post('detailDataPembelianDistrbutor', 'Api\RestController@getDetailBuyingDistributor');
    Route::get('ambilDataBudidayaPetani', 'Api\RestController@getDataCultivation');
    Route::get('grafikBudidayaPetani', 'Api\RestController@graphicCultivation');
    Route::get('listPabrik', 'Api\RestController@getDataFactory');
    Route::get('listFarm', 'Api\RestController@getDataFarm');
    Route::get('listUser', 'Api\RestController@getListUser');
    Route::get('listDataRole', 'Api\RestController@getDataRole');

});
