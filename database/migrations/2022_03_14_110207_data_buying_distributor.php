<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataBuyingDistributor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_buying_distributor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_users')->unsigned()->nullable()->default(null);
            $table->integer('total_item')->unsigned()->nullable()->default(null);
            $table->string('food_requirement')->nullable()->default(null);
            $table->date('date_delivery')->nullable()->default(null);
            $table->integer('createdby')->unsigned()->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_buying_distributor');
    }
}
