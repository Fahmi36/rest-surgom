<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataCultivation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_cultivation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('land_area')->unsigned()->nullable()->default(null);
            $table->string('type_product', 100)->nullable()->default(null);
            $table->integer('total_seed')->unsigned()->nullable()->default(null);
            $table->string('total_urea', 100)->nullable()->default(null);
            $table->string('total_sp36', 100)->nullable()->default(null);
            $table->string('total_kci', 100)->nullable()->default(null);
            $table->string('total_manure', 100)->nullable()->default(null);
            $table->date('date_first')->nullable()->default(null);
            $table->date('date_last')->nullable()->default(null);
            $table->integer('id_users')->unsigned()->nullable()->default(null);
            $table->date('date_harvest')->nullable()->default(null);
            $table->integer('createdby')->unsigned()->nullable()->default(12);
            $table->string('file_land')->nullable()->default(null);
            $table->string('file_planting')->nullable()->default(null);
            $table->string('file_fertilization')->nullable()->default(null);
            $table->string('file_thinning')->nullable()->default(null);
            $table->string('file_formation')->nullable()->default(null);
            $table->string('file_irrigation')->nullable()->default(null);
            $table->string('file_harvest')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_cultivation');
    }
}
